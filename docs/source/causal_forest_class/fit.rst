===
fit
===

.. currentmodule:: cforest.forest

.. automethod:: cforest.forest.CausalForest.fit
