====
load
====

.. currentmodule:: cforest.forest

.. automethod:: cforest.forest.CausalForest.load
