====
save
====

.. currentmodule:: cforest.forest

.. automethod:: cforest.forest.CausalForest.save
