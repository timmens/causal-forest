=======
predict
=======

.. currentmodule:: cforest.forest

.. automethod:: cforest.forest.CausalForest.predict
